'use strict';

exports.__esModule = true;
exports.default = stateFromHTML;

var _index = require('../index');

var _parseHTML = require('./parseHTML');

var _parseHTML2 = _interopRequireDefault(_parseHTML);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function stateFromHTML(html, options) {
  var parser = options == null || options.parser == null ? _parseHTML2.default : options.parser;
  var element = parser(html);
  return (0, _index.stateFromElement)(element, options);
}
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(stateFromHTML, 'stateFromHTML', 'src/editor/utils/stateFromHTML/main.js');
  leaveModule(module);
})();

;