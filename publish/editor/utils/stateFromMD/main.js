'use strict';

exports.__esModule = true;
exports.default = stateFromMarkdown;

var _MarkdownParser = require('./MarkdownParser');

var _MarkdownParser2 = _interopRequireDefault(_MarkdownParser);

var _index = require('../index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function stateFromMarkdown(markdown) {
  var element = _MarkdownParser2.default.parse(markdown, { getAST: true });
  return (0, _index.stateFromElement)(element);
}
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(stateFromMarkdown, 'stateFromMarkdown', 'src/editor/utils/stateFromMD/main.js');
  leaveModule(module);
})();

;