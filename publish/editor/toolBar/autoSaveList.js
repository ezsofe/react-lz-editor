'use strict';

var _redboxReact2 = require('redbox-react');

var _redboxReact3 = _interopRequireDefault(_redboxReact2);

var _reactTransformCatchErrors3 = require('react-transform-catch-errors');

var _reactTransformCatchErrors4 = _interopRequireDefault(_reactTransformCatchErrors3);

var _react2 = require('react');

var _react3 = _interopRequireDefault(_react2);

var _reactTransformHmr3 = require('react-transform-hmr');

var _reactTransformHmr4 = _interopRequireDefault(_reactTransformHmr3);

var _antd = require('antd');

var _publicDatas = require('../../global/supports/publicDatas');

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _components = {
  AutoSaveControls: {
    displayName: 'AutoSaveControls'
  }
};

var _reactTransformHmr2 = (0, _reactTransformHmr4.default)({
  filename: 'src/editor/toolBar/autoSaveList.jsx',
  components: _components,
  locals: [module],
  imports: [_react3.default]
});

var _reactTransformCatchErrors2 = (0, _reactTransformCatchErrors4.default)({
  filename: 'src/editor/toolBar/autoSaveList.jsx',
  components: _components,
  locals: [],
  imports: [_react3.default, _redboxReact3.default]
});

function _wrapComponent(id) {
  return function (Component) {
    return _reactTransformHmr2(_reactTransformCatchErrors2(Component, id), id);
  };
}

var AutoSaveControls = _wrapComponent('AutoSaveControls')(function (_Component) {
  _inherits(AutoSaveControls, _Component);

  function AutoSaveControls(props) {
    _classCallCheck(this, AutoSaveControls);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _this.state = {
      visible: false,
      list: [],
      selectedRowKeys: [],
      selectedKeyName: ""
    }, _this.onAutoSaveToggle = _this.onAutoSaveToggle.bind(_this);
    _this.handleCancel = _this.handleCancel.bind(_this);
    _this.sendSavedItemToEditor = _this.sendSavedItemToEditor.bind(_this);
    _this.doDelete = _this.doDelete.bind(_this);
    _this.selectRow = _this.selectRow.bind(_this);
    return _this;
  }

  AutoSaveControls.prototype.onAutoSaveToggle = function onAutoSaveToggle() {
    this.setState({ visible: true, list: [] });
    this.componentDidMount();
  };

  AutoSaveControls.prototype.doDelete = function doDelete(text) {
    window.localStorage.removeItem("$d" + text);
    var currItem = (0, _find2.default)(this.state.list, function (item) {
      return item.keyName == text;
    });
    if (currItem.key === this.state.selectedRowKeys[0]) {
      this.state.selectedRowKeys = [];
      this.forceUpdate();
    }
    this.componentDidMount();
  };

  AutoSaveControls.prototype.handleCancel = function handleCancel(e) {
    this.setState({ visible: false });
    this.state.list = [];
    this.forceUpdate();
  };

  AutoSaveControls.prototype.sendSavedItemToEditor = function sendSavedItemToEditor() {
    this.setState({ visible: false });
    var list = this.state.list.map(function (item) {
      return item;
    });
    var content = _publicDatas.PRO_COMMON.localDB.getter("$d" + this.state.selectedKeyName);
    this.props.receiveSavedItem(content);
    this.state.list = [];
    this.forceUpdate();
  };

  AutoSaveControls.prototype.componentDidMount = function componentDidMount() {
    var itemList = [];
    for (var i = 0; i < localStorage.length; i++) {
      var keyName = localStorage.key(i);
      if (!~keyName.lastIndexOf("$d")) {
        continue;
      }

      itemList.push({ keyName: keyName.replace("$d", "") });
    }

    _publicDatas.PRO_COMMON.obj.refsKeyTo(itemList);
    if (!!itemList.length) {
      this.setState({ list: itemList });
    } else {
      this.setState({ list: [] });
    }
  };

  AutoSaveControls.prototype.selectRow = function selectRow(record, index) {
    this.state.selectedRowKeys = [this.state.list[index].key];
    this.state.selectedKeyName = this.state.list[index].keyName;
    this.forceUpdate();
  };

  AutoSaveControls.prototype.render = function render() {
    var _this2 = this;

    var className = 'RichEditor-styleButton';
    var that = this;

    var columns = [{
      title: this.props.lang.previewMsg,
      dataIndex: 'keyName',
      key: 'keyName',
      render: function render(text, record, index) {
        return text + "...";
      }
    }, {
      title: '',
      key: 'operation',
      render: function render(text, record) {
        return _react3.default.createElement(
          'a',
          { onClick: function onClick() {
              return _this2.doDelete(record.keyName);
            } },
          _this2.props.lang.deleteDraftItem
        );
      }
    }];

    var rowSelection = {
      selectedRowKeys: that.state.selectedRowKeys,
      onChange: function onChange(selectedRowKeys, selectedRows) {
        that.state.selectedRowKeys = selectedRowKeys;
        that.forceUpdate();
      },
      onSelect: function onSelect(record, selected, selectedRows) {
        that.state.selectedKeyName = record.keyName;
      },
      type: "radio"
    };
    return _react3.default.createElement(
      'div',
      { className: 'RichEditor-controls' },
      _react3.default.createElement(
        'span',
        null,
        _react3.default.createElement(
          'span',
          { className: className, onClick: that.onAutoSaveToggle, title: this.props.lang.draftTipMsg },
          _react3.default.createElement(_antd.Icon, { type: 'editor_safty' })
        )
      ),
      _react3.default.createElement(
        _antd.Modal,
        {
          title: this.props.lang.draftModalTitle,
          visible: that.state.visible,
          closable: false,
          width: 600,
          footer: [_react3.default.createElement(
            _antd.Button,
            { key: 'back', size: 'large', onClick: that.handleCancel },
            '  ',
            this.props.lang.cancelText,
            '  '
          ), _react3.default.createElement(
            _antd.Popconfirm,
            { placement: 'right', title: this.props.lang.confirmUseDraft, onConfirm: that.sendSavedItemToEditor },
            '\xA0\xA0\xA0\xA0',
            _react3.default.createElement(
              _antd.Button,
              { key: 'submit', type: 'primary', size: 'large', disabled: !that.state.selectedRowKeys.length },
              ' ',
              this.props.lang.OKText,
              ' '
            )
          )] },
        _react3.default.createElement(_antd.Table, {
          rowSelection: rowSelection,
          onRowClick: that.selectRow,
          columns: columns,
          dataSource: that.state.list,
          size: 'small'
        }),
        _react3.default.createElement(
          'span',
          { style: { color: "#ccc" } },
          this.props.lang.draftCautionMsg
        )
      )
    );
  };

  AutoSaveControls.prototype.__reactstandin__regenerateByEval = function __reactstandin__regenerateByEval(key, code) {
    this[key] = eval(code);
  };

  return AutoSaveControls;
}(_react2.Component));

module.exports = AutoSaveControls;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(AutoSaveControls, 'AutoSaveControls', 'src/editor/toolBar/autoSaveList.jsx');
  leaveModule(module);
})();

;