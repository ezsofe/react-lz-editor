"use strict";

var _redboxReact2 = require("redbox-react");

var _redboxReact3 = _interopRequireDefault(_redboxReact2);

var _reactTransformCatchErrors3 = require("react-transform-catch-errors");

var _reactTransformCatchErrors4 = _interopRequireDefault(_reactTransformCatchErrors3);

var _react2 = require("react");

var _react3 = _interopRequireDefault(_react2);

var _reactTransformHmr3 = require("react-transform-hmr");

var _reactTransformHmr4 = _interopRequireDefault(_reactTransformHmr3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _components = {
  AutoSave: {
    displayName: "AutoSave"
  }
};

var _reactTransformHmr2 = (0, _reactTransformHmr4.default)({
  filename: "src/editor/toolBar/autoSave.jsx",
  components: _components,
  locals: [module],
  imports: [_react3.default]
});

var _reactTransformCatchErrors2 = (0, _reactTransformCatchErrors4.default)({
  filename: "src/editor/toolBar/autoSave.jsx",
  components: _components,
  locals: [],
  imports: [_react3.default, _redboxReact3.default]
});

function _wrapComponent(id) {
  return function (Component) {
    return _reactTransformHmr2(_reactTransformCatchErrors2(Component, id), id);
  };
}

var AutoSave = _wrapComponent("AutoSave")(function (_Component) {
  _inherits(AutoSave, _Component);

  function AutoSave(props) {
    _classCallCheck(this, AutoSave);

    return _possibleConstructorReturn(this, _Component.call(this, props));
  }

  AutoSave.prototype.render = function render() {
    return _react3.default.createElement(
      "div",
      { className: "RichEditor-controls" },
      _react3.default.createElement(
        "span",
        { className: "RichEditor-styleButton", onClick: this.props.onToggle },
        this.props.lang.autoSave
      )
    );
  };

  AutoSave.prototype.__reactstandin__regenerateByEval = function __reactstandin__regenerateByEval(key, code) {
    this[key] = eval(code);
  };

  return AutoSave;
}(_react2.Component));

module.exports = AutoSave;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(AutoSave, "AutoSave", "src/editor/toolBar/autoSave.jsx");
  leaveModule(module);
})();

;