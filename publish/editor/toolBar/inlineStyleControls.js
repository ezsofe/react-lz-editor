'use strict';

var _redboxReact2 = require('redbox-react');

var _redboxReact3 = _interopRequireDefault(_redboxReact2);

var _reactTransformCatchErrors3 = require('react-transform-catch-errors');

var _reactTransformCatchErrors4 = _interopRequireDefault(_reactTransformCatchErrors3);

var _react2 = require('react');

var _react3 = _interopRequireDefault(_react2);

var _reactTransformHmr3 = require('react-transform-hmr');

var _reactTransformHmr4 = _interopRequireDefault(_reactTransformHmr3);

var _styleButton = require('./styleButton');

var _styleButton2 = _interopRequireDefault(_styleButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _components = {
  InlineStyleControls: {
    displayName: 'InlineStyleControls'
  }
};

var _reactTransformHmr2 = (0, _reactTransformHmr4.default)({
  filename: 'src/editor/toolBar/inlineStyleControls.jsx',
  components: _components,
  locals: [module],
  imports: [_react3.default]
});

var _reactTransformCatchErrors2 = (0, _reactTransformCatchErrors4.default)({
  filename: 'src/editor/toolBar/inlineStyleControls.jsx',
  components: _components,
  locals: [],
  imports: [_react3.default, _redboxReact3.default]
});

function _wrapComponent(id) {
  return function (Component) {
    return _reactTransformHmr2(_reactTransformCatchErrors2(Component, id), id);
  };
}

var InlineStyleControls = _wrapComponent('InlineStyleControls')(function (_Component) {
  _inherits(InlineStyleControls, _Component);

  function InlineStyleControls(props) {
    _classCallCheck(this, InlineStyleControls);

    return _possibleConstructorReturn(this, _Component.call(this, props));
  }

  InlineStyleControls.prototype.render = function render() {
    var _props = this.props,
        editorState = _props.editorState,
        onToggle = _props.onToggle,
        lang = _props.lang;

    var INLINE_STYLES = [{
      text: lang.textBold,
      style: 'BOLD',
      label: "editor_b"
    }, {
      text: lang.textItalic,
      style: 'ITALIC',
      label: "editor_i"
    }, {
      text: lang.textUnderline,
      style: 'UNDERLINE',
      label: "editor_u"
    }, {
      text: lang.textCode,
      style: 'CODE',
      label: "editor_e"
    }, {
      text: lang.textStrikethrough,
      style: 'STRIKETHROUGH',
      label: 'editor_s'
    }];
    var currentStyle = editorState ? editorState.getCurrentInlineStyle() : {};
    return _react3.default.createElement(
      'div',
      { className: 'RichEditor-controls' },
      INLINE_STYLES.map(function (type, i) {
        return _react3.default.createElement(_styleButton2.default, {
          key: type.style,
          text: type.text,
          active: currentStyle.has(type.style),
          label: type.label,
          onToggle: onToggle,
          style: type.style });
      })
    );
  };

  InlineStyleControls.prototype.__reactstandin__regenerateByEval = function __reactstandin__regenerateByEval(key, code) {
    this[key] = eval(code);
  };

  return InlineStyleControls;
}(_react2.Component));

module.exports = InlineStyleControls;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(InlineStyleControls, 'InlineStyleControls', 'src/editor/toolBar/inlineStyleControls.jsx');
  leaveModule(module);
})();

;