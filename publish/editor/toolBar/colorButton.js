'use strict';

var _redboxReact2 = require('redbox-react');

var _redboxReact3 = _interopRequireDefault(_redboxReact2);

var _reactTransformCatchErrors3 = require('react-transform-catch-errors');

var _reactTransformCatchErrors4 = _interopRequireDefault(_reactTransformCatchErrors3);

var _react2 = require('react');

var _react3 = _interopRequireDefault(_react2);

var _reactTransformHmr3 = require('react-transform-hmr');

var _reactTransformHmr4 = _interopRequireDefault(_reactTransformHmr3);

var _colorStyleMap = require('../config/colorStyleMap');

var _colorStyleMap2 = _interopRequireDefault(_colorStyleMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _components = {
  ColorButton: {
    displayName: 'ColorButton'
  }
};

var _reactTransformHmr2 = (0, _reactTransformHmr4.default)({
  filename: 'src/editor/toolBar/colorButton.jsx',
  components: _components,
  locals: [module],
  imports: [_react3.default]
});

var _reactTransformCatchErrors2 = (0, _reactTransformCatchErrors4.default)({
  filename: 'src/editor/toolBar/colorButton.jsx',
  components: _components,
  locals: [],
  imports: [_react3.default, _redboxReact3.default]
});

function _wrapComponent(id) {
  return function (Component) {
    return _reactTransformHmr2(_reactTransformCatchErrors2(Component, id), id);
  };
}

var ColorButton = _wrapComponent('ColorButton')(function (_Component) {
  _inherits(ColorButton, _Component);

  function ColorButton(props) {
    _classCallCheck(this, ColorButton);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _this.onToggle = function (e) {
      e.preventDefault();
      _this.props.onToggle(_this.props.style);
    };
    return _this;
  }

  ColorButton.prototype.render = function render() {
    var _styles,
        _this2 = this;

    var styles = (_styles = {
      editor: {
        borderTop: '1px solid #ddd',
        cursor: 'text',
        fontSize: 16,
        marginTop: 20,
        minHeight: 400,
        paddingTop: 20
      },
      controls: {
        fontFamily: '\'Helvetica\', sans-serif',
        fontSize: 14,
        marginBottom: 10,
        userSelect: 'none'
      },
      ColorButton: {
        color: '#999',
        cursor: 'pointer',
        marginRight: 16,
        padding: '2px 0'
      },
      root: {
        fontFamily: '\'Georgia\', serif',
        padding: 20,
        width: 600
      },
      buttons: {
        marginBottom: 10
      },
      urlInputContainer: {
        marginBottom: 10
      },
      urlInput: {
        fontFamily: '\'Georgia\', serif',
        marginRight: 10,
        padding: 3
      }
    }, _styles['editor'] = {
      border: '1px solid #ccc',
      cursor: 'text',
      minHeight: 80,
      padding: 10
    }, _styles.button = {
      marginTop: 10,
      textAlign: 'center'
    }, _styles.link = {
      color: 'blue',
      textDecoration: 'underline'
    }, _styles);

    var style = Object.assign({}, styles.ColorButton, this.props.active ? _colorStyleMap2.default[this.props.style] : {});
    var className = 'RichEditor-styleButton';
    return _react3.default.createElement(
      'div',
      { className: 'RichEditor-color' },
      _react3.default.createElement(
        'span',
        {
          className: className,
          onClick: this.onToggle,
          style: {
            backgroundColor: _colorStyleMap2.default[this.props.style].color
          } },
        this.props.label
      ),
      function () {
        if (!!_this2.props.split) {
          return _react3.default.createElement(
            'span',
            { className: 'RichEditor-controls-split' },
            _this2.props.split
          );
        }
      }()
    );
  };

  ColorButton.prototype.__reactstandin__regenerateByEval = function __reactstandin__regenerateByEval(key, code) {
    this[key] = eval(code);
  };

  return ColorButton;
}(_react2.Component));

module.exports = ColorButton;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ColorButton, 'ColorButton', 'src/editor/toolBar/colorButton.jsx');
  leaveModule(module);
})();

;