'use strict';

exports.__esModule = true;

var _VideoSpan = require('./VideoSpan');

var _VideoSpan2 = _interopRequireDefault(_VideoSpan);

var _draftJs = require('draft-js');

var _main = require('../utils/stateUtils/main');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function findVideoEntities(contentBlock, callback) {
  contentBlock.findEntityRanges(function (character) {
    var entityKey = character.getEntity();
    return entityKey != null && _draftJs.Entity.get(entityKey).getType() === _main.ENTITY_TYPE.VIDEO;
  }, callback);
}

var _default = {
  strategy: findVideoEntities,
  component: _VideoSpan2.default
};
exports.default = _default;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(findVideoEntities, 'findVideoEntities', 'src/editor/decorators/VideoDecorator.jsx');
  reactHotLoader.register(_default, 'default', 'src/editor/decorators/VideoDecorator.jsx');
  leaveModule(module);
})();

;