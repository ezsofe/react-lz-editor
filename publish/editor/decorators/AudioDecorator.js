'use strict';

exports.__esModule = true;

var _AudioSpan = require('./AudioSpan');

var _AudioSpan2 = _interopRequireDefault(_AudioSpan);

var _draftJs = require('draft-js');

var _main = require('../utils/stateUtils/main');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function findAudioEntities(contentBlock, callback) {

  contentBlock.findEntityRanges(function (character) {
    var entityKey = character.getEntity();
    return entityKey != null && _draftJs.Entity.get(entityKey).getType() === _main.ENTITY_TYPE.AUDIO;
  }, callback);
}

var _default = {
  strategy: findAudioEntities,
  component: _AudioSpan2.default
};
exports.default = _default;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(findAudioEntities, 'findAudioEntities', 'src/editor/decorators/AudioDecorator.jsx');
  reactHotLoader.register(_default, 'default', 'src/editor/decorators/AudioDecorator.jsx');
  leaveModule(module);
})();

;