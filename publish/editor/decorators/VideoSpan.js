'use strict';

exports.__esModule = true;

var _redboxReact2 = require('redbox-react');

var _redboxReact3 = _interopRequireDefault(_redboxReact2);

var _reactTransformCatchErrors3 = require('react-transform-catch-errors');

var _reactTransformCatchErrors4 = _interopRequireDefault(_reactTransformCatchErrors3);

var _react2 = require('react');

var _react3 = _interopRequireDefault(_react2);

var _reactTransformHmr3 = require('react-transform-hmr');

var _reactTransformHmr4 = _interopRequireDefault(_reactTransformHmr3);

var _draftJs = require('draft-js');

var _decoratorStyle = require('./decoratorStyle.css');

var _decoratorStyle2 = _interopRequireDefault(_decoratorStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _components = {
  VideoSpan: {
    displayName: 'VideoSpan'
  }
};

var _reactTransformHmr2 = (0, _reactTransformHmr4.default)({
  filename: 'src/editor/decorators/VideoSpan.jsx',
  components: _components,
  locals: [module],
  imports: [_react3.default]
});

var _reactTransformCatchErrors2 = (0, _reactTransformCatchErrors4.default)({
  filename: 'src/editor/decorators/VideoSpan.jsx',
  components: _components,
  locals: [],
  imports: [_react3.default, _redboxReact3.default]
});

function _wrapComponent(id) {
  return function (Component) {
    return _reactTransformHmr2(_reactTransformCatchErrors2(Component, id), id);
  };
}

var VideoSpan = _wrapComponent('VideoSpan')(function (_Component) {
  _inherits(VideoSpan, _Component);

  function VideoSpan(props) {
    _classCallCheck(this, VideoSpan);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    var entity = _draftJs.Entity.get(_this.props.entityKey);

    var _entity$getData = entity.getData(),
        width = _entity$getData.width,
        height = _entity$getData.height;

    _this.state = {
      width: width,
      height: height
    };
    return _this;
  }

  VideoSpan.prototype.componentDidMount = function componentDidMount() {
    var _this2 = this;

    var _state = this.state,
        width = _state.width,
        height = _state.height;

    var entity = _draftJs.Entity.get(this.props.entityKey);
    var video = document.createElement('video');

    var _entity$getData2 = entity.getData(),
        src = _entity$getData2.src;

    video.src = src;
    video.onload = function () {
      if (width == null || height == null) {
        _this2.setState({ width: video.width, height: video.height });
        _draftJs.Entity.mergeData(_this2.props.entityKey, {
          width: video.width,
          height: video.height,
          originalWidth: video.width,
          originalHeight: video.height
        });
      }
    };
  };

  VideoSpan.prototype.render = function render() {
    var _state2 = this.state,
        width = _state2.width,
        height = _state2.height;

    var entity = _draftJs.Entity.get(this.props.entityKey);

    var _entity$getData3 = entity.getData(),
        src = _entity$getData3.src;

    var videoStyle = {
      verticalAlign: 'bottom',
      backgroundImage: 'url("' + src + '")',
      backgroundSize: width + 'px ' + height + 'px',
      lineHeight: height + 'px',
      fontSize: height + 'px',
      width: width,
      height: height,
      letterSpacing: width
    };

    return _react3.default.createElement(
      'figure',
      { className: 'editor-inline-video', onClick: this._onClick },
      _react3.default.createElement('video', { controls: 'controls', src: '' + src, className: 'media-video' })
    );
  };

  VideoSpan.prototype._onClick = function _onClick() {};

  VideoSpan.prototype._handleResize = function _handleResize(event, data) {
    var _data$size = data.size,
        width = _data$size.width,
        height = _data$size.height;

    this.setState({ width: width, height: height });
    _draftJs.Entity.mergeData(this.props.entityKey, { width: width, height: height });
  };

  VideoSpan.prototype.__reactstandin__regenerateByEval = function __reactstandin__regenerateByEval(key, code) {
    this[key] = eval(code);
  };

  return VideoSpan;
}(_react2.Component));

var _default = VideoSpan;
exports.default = _default;


VideoSpan.defaultProps = {
  children: null,
  entityKey: "",
  className: ""
};
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(VideoSpan, 'VideoSpan', 'src/editor/decorators/VideoSpan.jsx');
  reactHotLoader.register(_default, 'default', 'src/editor/decorators/VideoSpan.jsx');
  leaveModule(module);
})();

;