'use strict';

exports.__esModule = true;

var _ImageSpan = require('./ImageSpan');

var _ImageSpan2 = _interopRequireDefault(_ImageSpan);

var _draftJs = require('draft-js');

var _main = require('../utils/stateUtils/main');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function findImageEntities(contentBlock, callback) {
  contentBlock.findEntityRanges(function (character) {
    var entityKey = character.getEntity();
    return entityKey != null && _draftJs.Entity.get(entityKey).getType() === _main.ENTITY_TYPE.IMAGE;
  }, callback);
}

var _default = {
  strategy: findImageEntities,
  component: _ImageSpan2.default
};
exports.default = _default;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(findImageEntities, 'findImageEntities', 'src/editor/decorators/ImageDecorator.jsx');
  reactHotLoader.register(_default, 'default', 'src/editor/decorators/ImageDecorator.jsx');
  leaveModule(module);
})();

;