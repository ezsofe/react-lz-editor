'use strict';

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _draftJs = require('draft-js');

var _main = require('../utils/stateUtils/main');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

function Link(props_) {
  var _Entity$get$getData = _draftJs.Entity.get(props_.entityKey).getData(),
      url = _Entity$get$getData.url;

  return _react2.default.createElement(
    'a',
    { href: url },
    props_.children
  );
}

function findLinkEntities(contentBlock, callback) {
  contentBlock.findEntityRanges(function (character) {
    var entityKey = character.getEntity();
    return entityKey != null && _draftJs.Entity.get(entityKey).getType() === _main.ENTITY_TYPE.LINK;
  }, callback);
}

var _default = {
  strategy: findLinkEntities,
  component: Link
};
exports.default = _default;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Link, 'Link', 'src/editor/decorators/LinkDecorator.jsx');
  reactHotLoader.register(findLinkEntities, 'findLinkEntities', 'src/editor/decorators/LinkDecorator.jsx');
  reactHotLoader.register(_default, 'default', 'src/editor/decorators/LinkDecorator.jsx');
  leaveModule(module);
})();

;