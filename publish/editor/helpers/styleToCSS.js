'use strict';

exports.__esModule = true;

var _CSSProperty = require('react-dom/lib/CSSProperty');

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var VENDOR_PREFIX = /^(moz|ms|o|webkit)-/;
var NUMERIC_STRING = /^\d+$/;
var UPPERCASE_PATTERN = /([A-Z])/g;

function processStyleName(name) {
  return name.replace(UPPERCASE_PATTERN, '-$1').toLowerCase().replace(VENDOR_PREFIX, '-$1-');
}

function processStyleValue(name, value) {
  var isNumeric = void 0;
  if (typeof value === 'string') {
    isNumeric = NUMERIC_STRING.test(value);
  } else {
    isNumeric = true;
    value = String(value);
  }
  if (!isNumeric || value === '0' || _CSSProperty.isUnitlessNumber[name] === true) {
    return value;
  } else {
    return value + 'px';
  }
}

function styleToCSS(styleDescr) {
  return Object.keys(styleDescr).map(function (name) {
    var styleValue = processStyleValue(name, styleDescr[name]);
    var styleName = processStyleName(name);
    return styleName + ': ' + styleValue;
  }).join('; ');
}

var _default = styleToCSS;
exports.default = _default;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(VENDOR_PREFIX, 'VENDOR_PREFIX', 'src/editor/helpers/styleToCSS.js');
  reactHotLoader.register(NUMERIC_STRING, 'NUMERIC_STRING', 'src/editor/helpers/styleToCSS.js');
  reactHotLoader.register(UPPERCASE_PATTERN, 'UPPERCASE_PATTERN', 'src/editor/helpers/styleToCSS.js');
  reactHotLoader.register(processStyleName, 'processStyleName', 'src/editor/helpers/styleToCSS.js');
  reactHotLoader.register(processStyleValue, 'processStyleValue', 'src/editor/helpers/styleToCSS.js');
  reactHotLoader.register(styleToCSS, 'styleToCSS', 'src/editor/helpers/styleToCSS.js');
  reactHotLoader.register(_default, 'default', 'src/editor/helpers/styleToCSS.js');
  leaveModule(module);
})();

;